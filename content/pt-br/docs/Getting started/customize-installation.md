---
title: "Personalizar a instalação"
linkTitle: "Personalizar a instalação"
weight: 4
description: >
  Como personalizar a interface web!
---

> Mudanças simples que podem ser implementadas no layout de um site OpenDataBio

## Logo e imagem de fundo

Para substituir o logotipo da barra de navegação e a imagem da página inicial,
apenas coloque seus arquivos de imagem substituindo os arquivos em `/public/custom/` sem alterar seus nomes.

## Textos e informações

Para alterar o texto de boas-vindas da página inicial, altere os valores para cada entrada nos arquivos:
* `/resources/lang/en/customs.php`
* `/resources/lang/pt/customs.php`
* Não remova as chaves de entrada. Defina como `null` para suprimir a exibição no rodapé e na página inicial.


## NavBar e Rodapé

1. Se você deseja alterar a cor da barra de navegação superior e do rodapé,
basta substituir a classe css Boostrap 5 nas tags e arquivos correspondentes na pasta `/resources/view/layout`.
1. Você pode adicionar html adicional ao rodapé e barra de navegação, alterar o tamanho do logotipo, etc... como desejar.
