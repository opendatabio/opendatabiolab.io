---
title: "Atualizar dados - PUT"
linkTitle: "Atualizar dados - PUT"
weight: 4
description: >
  Como atualizar dados no OpenDataBio usando a API
---

{{< alert color="primary" title="Atenção">}}
Somente os endpoints listados abaixo podem ser atualizados usando a API e somente os campos PUT listados podem ser atualizados em cada endpoint. Os valores dos campos são os mesmos como explicados para a API POST, exceto que **em todos os casos o `ID` do registro a ser atualizado também deve ser fornecido**.
{{< /alert >}}

| Endpoint | Description | PUT Fields |
| --- | --- | --- |
| [individuals](/docs/api/post-data/#post-individuals) | Atualizar Indivíduos | (`id` or `individual_id`),`collector`, `tag`, `dataset`, `date`, `notes `, `taxon `, `identifier `, `identification_date `, `modifier`, `identification_notes `, `identification_based_on_biocollection`, `identification_based_on_biocollection_id `, `identification_individual` |
| [individual-locations](/docs/api/post-data/#post-individual-locations) | Atualizar Localidades de Indivíduos | (`id` or `individual_location_id`), `individual`, (`location` or `latitude` + `longitude`), `altitude`, `location_notes`, `location_date_time`, `x`, `y`, `distance`, `angle`|
| [locations](/docs/api/post-data/#post-locations) | Atualizar Localidades |  (`id` or `location_id`), `name`, `adm_level`, (`geom` or `lat`+`long`) , `parent`, `altitude`, `datum`, `x`, `y` , `startx`, `starty`, `notes`, `ismarine` |
| [measurements](/docs/api/post-data/#post-measurements)  | Atualizar Medições | (`id` or `measurement_id`), `dataset`, `date`, `object_type`, `object_type`, `person`, `trait_id`, `value`, `link_id`, `bibreference`, `notes`, `duplicated`,`parent_measurement`|
| [persons](/docs/api/post-data/#post-persons)  | Atualizar Pessoas |(`id` or `person_id`),`full_name`, `abbreviation`, `email`, `institution`, `biocollection`|
| [vouchers](/docs/api/post-data/#post-vouchers)  | Atualizar Vouchers |(`id` or `voucher_id`),`individual`, `biocollection`, `biocollection_type`, `biocollection_number`, `number`, `collector`, `date`, `dataset`, `notes` |


