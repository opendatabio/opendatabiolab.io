---
title: "Update data - PUT"
linkTitle: "Update data - PUT"
weight: 4
description: >
  How to update data already in OpenDataBio!
---

{{< alert color="primary" title="Attention">}}
Only the endpoints listed below can be updated using the API and only the listed PUT fields can be updated on each endpoint. Field values are as explained for the POST API endpoints, except that **in all cases you must also provide the `ID` of the record to be updated**.
{{< /alert >}}

| Endpoint | Description | PUT Fields |
| --- | --- | --- |
| [individuals](/docs/api/post-data/#post-individuals) | Atualizar Indivíduos | (`id` or `individual_id`),`collector`, `tag`, `dataset`, `date`, `notes `, `taxon `, `identifier `, `identification_date `, `modifier`, `identification_notes `, `identification_based_on_biocollection`, `identification_based_on_biocollection_id `, `identification_individual` |
| [individual-locations](/docs/api/post-data/#post-individual-locations) | Atualizar Localidades de Indivíduos | (`id` or `individual_location_id`), `individual`, (`location` or `latitude` + `longitude`), `altitude`, `location_notes`, `location_date_time`, `x`, `y`, `distance`, `angle`|
| [locations](/docs/api/post-data/#post-locations) | Atualizar Localidades |  (`id` or `location_id`), `name`, `adm_level`, (`geom` or `lat`+`long`) , `parent`, `altitude`, `datum`, `x`, `y` , `startx`, `starty`, `notes`, `ismarine` |
| [measurements](/docs/api/post-data/#post-measurements)  | Atualizar Medições | (`id` or `measurement_id`), `dataset`, `date`, `object_type`, `object_type`, `person`, `trait_id`, `value`, `link_id`, `bibreference`, `notes`, `duplicated`,`parent_measurement`|
| [persons](/docs/api/post-data/#post-persons)  | Atualizar Pessoas |(`id` or `person_id`),`full_name`, `abbreviation`, `email`, `institution`, `biocollection`|
| [vouchers](/docs/api/post-data/#post-vouchers)  | Atualizar Vouchers |(`id` or `voucher_id`),`individual`, `biocollection`, `biocollection_type`, `biocollection_number`, `number`, `collector`, `date`, `dataset`, `notes` |


