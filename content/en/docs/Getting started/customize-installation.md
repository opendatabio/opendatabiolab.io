---
title: "Customize Installation"
linkTitle: "Customize Installation"
weight: 4
description: >
  How to customize the web interface!
---

> Simple changes that can be implemented in the layout of a OpenDataBio web site

## Logo and BackGround Image

To replace the Navigation bar logo and the image of the landing page,
just put your image files replacing the files in `/public/custom/` without changing their names.

## Texts and Info

To change the welcome text of the landing page, change the values of the array keys in the following files:
* `/resources/lang/en/customs.php`
* `/resources/lang/pt/customs.php`  
* Do not remove the entry keys. Set to null to suppress from appearing in the footer and landing page.


## NavBar and Footer

1. If you want to change the color of the top navigation bar and the footer,
just replace css Boostrap 5 class in the corresponding tags and files in folder `/resources/view/layout`.
1. You may add additional html to the footer and navbar, change logo size, etc... as you wish.
