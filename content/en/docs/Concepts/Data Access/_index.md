---
title: "Data Access Objects"
linkTitle: "Data Access Objects"
weight: 3
description: >
  Objects controlling data access and distribution!
---

[Datasets](/en/docs/concepts/data-access/#dataset) control data access and represents a dynamic data publication, with a version defined by last edition date. Datasets may contain [Measurements](/en/docs/concepts/trait-objects/#measurement), [Individuals](/en/docs/concepts/core-objects/#individual), [Vouchers](/en/docs/concepts/core-objects/#voucher) and/or [Media Files](/en/docs/concepts/auxiliary-objects/#media).

[Projects](#projects) are just groups of [Datasets](/en/docs/concepts/data-access/#dataset) and [Users](/en/docs/concepts/data-access/#user), representing coohorts of users with common accessibility to datasets whose privacy are set to be controlled by a Project.

[BioCollections](#biocollection) - This model serves to create a reusable list of acronyms of Biological Collections to record [Vouchers](/en/docs/concepts/core-objects/#voucher). However, you can optionally manage a collection of [Vouchers](/en/docs/concepts/core-objects/#voucher) and their [Individuals](/en/docs/concepts/core-objects/#individual), in parallel to the access control provided by [Datasets](/en/docs/concepts/data-access/#dataset). Control is only for editing and entering Voucher records. In this case, the BioCollection is managed by the system.

Projects and BioCollections must have at least one [User](/en/docs/concepts/data-access/#user) defined as `administrator`, who has total control over the dataset or project, including granting the following roles to other users: `administrator`, `collaborator` or `viewer`:

* **Collaborators** are able to insert and edit objects, but are not able to delete records nor change the dataset or project configuration.
* **Viewers** have read-only access to the data that are not of open access.
* Only **Full Users** and **SuperAdmins** may be assigned as **administrators** or **collaborators**. Thus, if a user who was administrator or collaborator of a dataset is demoted to "Registered User", she or he will become a viewer.
* Only **Super-admins** can enable a BioCollection to be administered by the system.

***
<a name="biocollection"></a>
## Biocollections
The **Biocollection** model has two functions: (1) to provide a list of acronyms for registering [Vouchers](/en/docs/concepts/core-objects/#voucher) of any Biological Collection; (2) to manage the data of Biological Collections, facilitating the registration of new data (any user enters their data using the validations carried out by the software and requests the collection's curators through the interface to register the data, which is done by authorized users for the BioCollection. Upon data registration, the BioCollection controls the editing of the data of the [Vouchers](/en/docs/concepts/core-objects/#individual) and the related [Individuals](/en/docs/concepts/core-objects/#individual). Option (2) needs to be implemented by a Super-Administrator user, who can enable a BioCollection to be administered by the system, implementing the **ODBRequest** request model so that users can request data, samples, or records and changes to the data.

The Biocollection object may be a formal Biocollection, such as those registered in the Index Herbariorum (http://sweetgum.nybg.org/science/ih/), or any other Biological Collection, formal or informal.  

The Biocollection object also interacts with the [Person](/en/docs/concepts/auxiliary-objects/#person) model. When a Person is linked to an Biocollection it will be listed as a taxonomic specialist.

![](biocollection_model.png)

**Data access** - Full users can register BioCollections, but only super administrator can make a BioCollection manageable by the system. Removing BioCollections can be done if there are no Vouchers attached and if it is not administered by the system. If manageable, the model interacts with [Users](/en/docs/concepts/data-access/#user), which can be `administrators` (curators, anything can) or `collaborators` (can enter and edit data, but cannot delete records). Data from other Datasets can be part of the BioCollection, allowing users to have their complete data, but editing control of records is by the BioCollection authorized users.

***
<a name="dataset"></a>
## Datasets

**DataSets** are groups of [Measurements](/en/docs/concepts/trait-objects/#measurement), [Individuals](/en/docs/concepts/core-objects/#individual), [Vouchers](/en/docs/concepts/core-objects/#voucher) and/or [Media Files](/en/docs/concepts/auxiliary-objects/#media), and may have one or more [Users](/en/docs/concepts/data-access/#user)  `administrators`, `collaborators` or `viewers`. Administrators may set the `privacy level` to *public access*, *restricted to registered users* or *restricted to authorized users* or *restricted to project users*. This control access to the data within a dataset as exemplified in diagram below:

![](dataset_model.png)


Datasets may also have many [Bibliographic References](/en/docs/concepts/auxiliary-objects/#bibreference), which together with fields `policy`, `metadata` permits to annotate the dataset with relevant information for data sharing:
    * Link any publication that have used the dataset and optionally indicate that they are of mandatory citation when using the data;
    * Define a specific data `policy` when using the data in addition to the a <a href="https://creativecommons.org/licenses/">CreativeCommons.org</a> public `license`;
    * Detail any relevant `metadata` in addition to those that are automatically retrieved from the database like the definitions of the [Traits](/en/docs/concepts/trait-objects/#trait) measured.

![](dataset_bibreference.png)

***
<a name="project"></a>
## Projects

**Projects** are just groups of [Datasets](/en/docs/concepts/data-access/#dataset) and interacts with [Users](/en/docs/concepts/data-access/#user), having  `administrators`, `collaborators` or `viewers`. These users may control all datasets within the Project having a *restricted to project users* access policy.


![](project_model.png)

***
<a name="user"></a>
## Users

The **Users** table stores information about the database users and administrators. Each **User** may be associated with a default [Person](/en/docs/concepts/auxiliary-objects/#person). When this user enters new data, this person is used as the default person in forms. The person can only be associated to a single user.

There are three possible **access levels** for a user:
    * `Registered User` (the lowest level) - have very few permissions
    * `Full User` - may be assigned as administrators or collaborators to Projects and Datasets;
    * `SuperAdmin` (the highest level). - superadmins have have access to all objects, regardless of project or dataset configuration and is the system administrator.


![](user_model.png)


Each user is assigned to the **registered user** level when she or he registers in an OpenDataBio system. After that, a **SuperAdmin** may promote her/him to Full User or SuperAdmin. SuperAdmins also have the ability to edit other users and remove them from the database.

Every registered user is created along with a restricted Project and Dataset,  which are referred to as her **user Workspace**. This allows users to import individual and voucher data before incorporating them into a larger project. [TO IMPLEMENT: export batches of objects from one project to another].


**Data Access**:users are created upon registration. Only administrators can update and delete user records.


***
<a name="user-job"></a>
## User Jobs

The **UserJob** table is used to store temporarily background tasks, such as importing and exporting data. Any user is allowed to create a job; cancel their own jobs; list jobs that have not been deleted. The **Job** table contains the data used by the Laravel framework to interact with the Queue. The data from this table is deleted when the job runs successfully. The UserJob entity is used to keep this information, along with allowing for job logs, retrying failed jobs and canceling jobs that have not yet finished.


![](user_userjob.png)



**Data Access**: Each registered user can see, edit and remove their own UserJobs.
